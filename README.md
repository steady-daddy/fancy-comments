Fancy Comments
==

Setup

- Clone the repository: `git clone https://gitlab.com/steady-daddy/fancy-comments.git`
- Make sure you have npm, grunt and bower installed. If not:
   - install nodejs from the [official website](https://nodejs.org/en/download/)
   - install grunt: `npm install -g grunt-cli`
   - install bower: `npm install -g bower`
- Open up a console and browse to the project folder
- Install project dependencies:
    - `npm install`
    - `bower install`
- To start the local server form the console type: `grunt`
- Browse to `http://localhost:3000` to access the project.

Screenshots
==
![Alt text](fc1.png?raw=true "Keyword highlighting")
==
![Alt text](fc.png?raw=true "Full page screenshot")

